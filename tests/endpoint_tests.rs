use chrono::prelude::*;
use hafas_client;
use hafas_client::{Journey, Leg, StationBoard, TimeType};

#[tokio::main]
pub async fn endpoint_tests(
    endpoint_name: String,
    origin_str: String,
    via_str: String,
    destination_str: String,
) {
    let endpoints = hafas_client::Endpoint::infos().unwrap();
    let endpoint_index = endpoints
        .iter()
        .position(|endpoint| endpoint.name == endpoint_name)
        .unwrap();
    let client = hafas_client::Client::new(endpoint_index).unwrap();

    let origin_res = client.get_locations(origin_str.clone()).await;
    let origin = match origin_res {
        Ok(j) => j,
        Err(e) => panic!("Error getting Origin locations of {}: {}", endpoint_name, e),
    };
    let destination_res = client.get_locations(destination_str.clone()).await;
    let destination = match destination_res {
        Ok(j) => j,
        Err(e) => panic!(
            "Error getting Destination locations of {}: {}",
            endpoint_name, e
        ),
    };
    let via_res = client.get_locations(via_str).await;
    let via = match via_res {
        Ok(j) => j,
        Err(e) => panic!("Error getting via locations: {}", e),
    };

    client.set_departure_id(&Some(origin[0].clone()));
    client.set_journeys_via(&Some(via[0].clone()));
    client.set_arrival_id(&Some(destination[0].clone()));

    let journeys_result = client.get_journeys().await;
    let journeys = match journeys_result {
        Ok(j) => j,
        Err(e) => panic!("Error getting Journeys of {}: {}", endpoint_name, e),
    };

    // Check that journeys start later than the choosen time
    let journey_starttime_local = journeys.journeys[0].legs[0].plannedDeparture.unwrap();
    // let now_local = Utc::now().with_timezone(timezone).naive_local(); // TODO: get timezone from json file
    let now_local = Local::now().naive_local();
    assert!(
        journey_starttime_local.timestamp() + (60 * 15) > now_local.timestamp(),
        "First journey starts at: {}, Local time is: {}",
        journey_starttime_local,
        now_local
    );

    let client_arrival = client.clone();
    client_arrival.set_datetime_type(TimeType::Arrival);
    let journeys_result_arrival = client_arrival.get_journeys().await;
    let journeys_arrival = match journeys_result_arrival {
        Ok(j) => j,
        Err(e) => panic!("Error getting Journeys_arrival of {}: {}", endpoint_name, e),
    };

    // Check that journeys arrives earlier than the choosen time
    let journey_arrival = journeys_arrival.journeys[&journeys_arrival.journeys.len() - 1].legs
        [journeys_arrival.journeys[&journeys_arrival.journeys.len() - 1]
            .legs
            .len()
            - 1]
    .plannedArrival
    .unwrap();
    assert!(
        journey_arrival.timestamp() - (60 * 15) < now_local.timestamp(),
        "Last journey arrives at: {}, Local time is: {}, journey data: {:#?}",
        journey_arrival,
        now_local,
        journey_arrival
    );

    let journey: &Journey = &journeys.journeys[0];
    let journey_refreshed = client.refresh_journey(journey).await;
    match journey_refreshed {
        Ok(_j) => (),
        Err(e) => panic!(
            "Error getting refreshed Journey of {}: {}",
            endpoint_name, e
        ),
    };
    let _leg: &Leg = &journeys.journeys[0].legs[0];

    let earlier_journeys = client.get_earlier_journeys().await;
    match earlier_journeys {
        Ok(_j) => println!(),
        Err(e) => panic!("Error getting earlier Journeys of {}: {}", endpoint_name, e),
    };

    let later_journeys = client.get_later_journeys().await;
    match later_journeys {
        Ok(_j) => (),
        Err(e) => panic!("Error getting later Journeys of {}: {}", endpoint_name, e),
    };

    client.set_locations_type(hafas_client::LocationType::Station);
    let station_res = client.get_locations(destination_str).await;
    let station = match station_res {
        Ok(j) => j,
        Err(e) => panic!("Error getting Origin stations of {}: {}", endpoint_name, e),
    };
    client.set_stationboard_id(Some(
        station[1].id.as_ref().unwrap().parse::<i64>().unwrap(),
    ));
    assert!(client.get_stationboard_id().is_some());
    let stationboard_result = client.get_stationboard().await;
    let stationboards = match stationboard_result {
        Ok(s) => s,
        Err(e) => panic!("Error getting Stationboard of {}: {}", endpoint_name, e),
    };
    let _stationboard: &StationBoard = &stationboards[0];
}
