use env_logger;
use hafas_client;
use hafas_client::BoxError;
use hafas_client::{Journeys, Location};
use once_cell::sync::Lazy;

#[test]
fn structure_test() {
    // env_logger::init();
    structure_async();
}

#[tokio::main]
async fn structure_async() {
    let endpoint = hafas_client::Client::new(0).unwrap();

    let _journeys = Journeys::default();
    let _location: Location = Location::default();

    let origin = endpoint
        .get_locations("Frankfurt".to_string())
        .await
        .unwrap();
    let destination = endpoint.get_locations("Berlin".to_string()).await.unwrap();

    endpoint.set_departure_id(&Some(origin[0].clone()));
    let mut via = Location::default();
    via.id = Some(8000134.to_string());
    endpoint.set_journeys_via(&Some(via.clone()));
    endpoint.set_arrival_id(&Some(destination[0].clone()));

    let _departure_id: Location = endpoint.get_departure_id().unwrap();
    let _arrival_id: Location = endpoint.get_arrival_id().unwrap();
    let _stopover_id: Location = endpoint.get_journeys_via().unwrap();

    endpoint.set_journeys_via(&None);
    assert_eq!(endpoint.get_journeys_via().is_some(), false);

    let journeys_result = endpoint.get_journeys().await;
    match journeys_result {
        Ok(j) => j,
        Err(e) => panic!("Error: {}", e),
    };
}

#[test]
fn test_impossible_route() {
    env_logger::init();
    impossible_route_async();
}

#[tokio::main]
async fn impossible_route_async() {
    let endpoint = hafas_client::Client::new(0).unwrap();

    let origin = endpoint
        .get_locations("Köln HBF".to_string())
        .await
        .unwrap();
    let destination = endpoint
        .get_locations("Hauptwache, Frankfurt a.M.".to_string())
        .await
        .unwrap();

    endpoint.set_departure_id(&Some(origin[0].clone()));
    endpoint.set_arrival_id(&Some(destination[0].clone()));

    *endpoint.journeys_config.products_used[4]
        .used
        .lock()
        .unwrap() = false;
    *endpoint.journeys_config.products_used[5]
        .used
        .lock()
        .unwrap() = false;
    *endpoint.journeys_config.products_used[6]
        .used
        .lock()
        .unwrap() = false;
    *endpoint.journeys_config.products_used[7]
        .used
        .lock()
        .unwrap() = false;
    *endpoint.journeys_config.products_used[8]
        .used
        .lock()
        .unwrap() = false;

    let journeys_result = endpoint.get_journeys().await;
    match journeys_result {
        Ok(j) => println!("Later Journeys: {:#?}", j),
        Err(e) => println!("Expexted error: {}", e),
    };
}

#[test]
fn handle_test() {
    pub static RUNTIME: Lazy<tokio::runtime::Runtime> =
        Lazy::new(|| tokio::runtime::Runtime::new().unwrap());

    let client = hafas_client::Client::new(0).unwrap();
    let search_string = "Frankfurt".to_string();

    let _locations_handle: tokio::task::JoinHandle<Result<Vec<Location>, BoxError>> = RUNTIME
        .spawn(async move {
            let locations = client.get_locations(search_string).await;
            // std::thread::sleep(std::time::Duration::from_secs(2));
            locations
        });
}
