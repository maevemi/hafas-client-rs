use env_logger;

mod endpoint_tests;

#[test]
fn db_test() {
    env_logger::init();
    endpoint_tests::endpoint_tests(
        "Deutsche Bahn (DB)".to_string(),
        // "Frankfurt".to_string(),
        "Rütscherstraße 155".to_string(),
        "Köln".to_string(),
        "Berlin".to_string(),
    );
}

#[test]
fn cfl_test() {
    endpoint_tests::endpoint_tests(
        "Société Nationale des Chemins de Fer Luxembourgeois (CFL)".to_string(),
        "Ettelbruck".to_string(),
        "Mersch".to_string(),
        "Luxembourg Central".to_string(),
    );
}

// #[test]
// fn sncb_test() {
//     env_logger::init();
//     endpoint_tests::endpoint_tests(
//         "Belgian National Rail (SNCB/NMBS)".to_string(),
//         "Brussels".to_string(),
//         "Liege".to_string(),
//         "Namur".to_string(),
//     );
// }
