use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use std::error::Error;
extern crate hex;

use crate::models::{Journey, Location, StationBoard};
use crate::request::{
    ArrLocL, Auth, Cfg, ClientR, Ctx, DepLocL, Input, JnyFltrL, Loc, PostConfig, Rec, StbLoc,
    ViaLocL,
};
use crate::response::ServerReply;
use crate::{BoxError, Journeys, JourneysConfig, LocationsConfig, StationBoardConfig, TimeType};

// Structs to read api endpoint information

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct HafasType {
    pub hafasMgate: bool,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Attribution {
    pub name: String,
    pub homepage: String,
    pub isProprietary: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Region {
    pub region: Vec<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Coverage {
    pub realtimeCoverage: Region,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub regularCoverage: Option<Region>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub anyCoverage: Option<Region>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Product {
    pub id: String,
    pub bitmasks: Vec<u64>,
    pub name: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Options {
    pub auth: Auth,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub checksumSalt: Option<String>,
    pub client: ClientR,
    pub endpoint: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ext: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locationIdentifierType: Option<String>,
    pub products: Vec<Product>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub standardLocationIdentifierCountries: Option<Vec<u64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub standardLocationIdentifierType: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub journeyRefresh: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cfg: Option<Cfg>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Endpoint {
    pub name: String,
    pub r#type: HafasType,
    pub supportedLanguages: Vec<String>,
    pub timezone: String,
    pub attribution: Attribution,
    pub coverage: Coverage,
    pub options: Options,
}

impl Endpoint {
    pub fn infos() -> Result<Vec<Self>, Box<dyn Error>> {
        let mut endpoints = Vec::new();
        endpoints.push(serde_json::from_str(include_str!(
            "../endpoint-data/de/db-hafas-mgate.json"
        ))?);
        endpoints.push(serde_json::from_str(include_str!(
            "../endpoint-data/lu/cfl-hafas-mgate.json"
        ))?);
        endpoints.push(serde_json::from_str(include_str!(
            "../endpoint-data/be/nmbs-sncb-hafas-mgate.json"
        ))?);
        Ok(endpoints)
    }

    async fn server_request(&self, post_body: PostConfig) -> Result<ServerReply, BoxError> {
        log::trace!(
            "Post Request body: {}",
            serde_json::to_string_pretty(&post_body).unwrap()
        );

        let post_body_string = serde_json::to_string(&post_body).unwrap();

        // TODO: only initialize once and reuse
        // let client = reqwest::Client::builder().user_agent("PublicTransport").build().unwrap();

        let mut request = reqwest::Client::new()
            .post(&self.options.endpoint)
            .header(reqwest::header::CONTENT_TYPE, "application/json")
            .header(reqwest::header::ACCEPT_CHARSET, "utf-8")
            .header(
                reqwest::header::USER_AGENT,
                "https://gitlab.com/maevemi/publictransport",
            )
            .body(post_body_string.clone());

        if let Some(c) = self.options.checksumSalt.clone() {
            let salt_utf8 = hex::decode(c).unwrap();
            let salt = std::str::from_utf8(&salt_utf8).unwrap();
            let data_salt = post_body_string.to_string() + salt;
            let md5 = md5::compute(data_salt);

            request = request.query(&[("checksum", format!("{:x}", md5))]);
        }

        log::trace!("Post Request: {:#?}", request);

        let server_reply_r = request.send().await?;
        let server_reply_value = server_reply_r.json::<serde_json::Value>().await?;

        log::trace!(
            "server Reply for post request: {}",
            serde_json::to_string_pretty(&server_reply_value).unwrap()
        );

        let server_reply: ServerReply = serde_json::from_value(server_reply_value)?;
        Ok(server_reply)
    }

    pub async fn locations(
        &self,
        location: &str,
        locations_config: &LocationsConfig,
    ) -> Result<Vec<Location>, BoxError> {
        let location_type = match *locations_config.location_type.lock().unwrap() {
            crate::LocationType::All => "ALL".to_string(),
            crate::LocationType::Station => "S".to_string(),
        };

        let mut rec = Rec::new();
        rec.input = Some(Input {
            field: "S".to_string(),
            loc: Loc {
                lid: None,
                name: Some(location.to_string()),
                r#type: location_type,
                extId: None,
            },
        });

        let mut post_config = PostConfig::new(rec, self.clone());
        post_config.svcReqL[0].meth = Some("LocMatch".to_string());
        post_config.svcReqL[0].cfg = self.options.cfg.clone();

        let server_reply = self.server_request(post_config).await?;

        let mut locations = Location::new_locations(
            &server_reply.svcResL.as_ref().unwrap()[0]
                .res
                .r#match
                .as_ref()
                .unwrap()
                .locL,
        );

        if let Some(max_locations) = *locations_config.max_results.lock().unwrap() {
            if &locations.len() > &max_locations {
                locations = locations.drain(..max_locations).collect();
            }
        }

        Ok(locations)
    }

    #[allow(non_snake_case)]
    pub async fn update_journey(&self, refresh_token: &str) -> Result<Journey, BoxError> {
        let mut request = Rec::new();
        if let Some(journey_refresh) = &self.options.journeyRefresh {
            if journey_refresh == &"cfl".to_string() {
                request.outReconL = Some(vec![Ctx {
                    ctx: refresh_token.to_string(),
                }]);
                request.ctxRecon = None;
            } else {
                request.ctxRecon = Some(refresh_token.to_string());
                request.outReconL = None;
            }
        } else {
            request.ctxRecon = Some(refresh_token.to_string());
            request.outReconL = None;
        }

        let mut post_config = PostConfig::new(request, self.clone());
        post_config.svcReqL[0].meth = Some("Reconstruction".to_string());

        let server_reply = self.server_request(post_config).await?;

        let journey = Journey::new(
            &server_reply
                .svcResL
                .as_ref()
                .ok_or("res does not contain any journey")?[0]
                .res,
            &server_reply
                .svcResL
                .as_ref()
                .ok_or("res does not contain any journey")?[0]
                .res
                .outConL
                .as_ref()
                .ok_or("res.outConL does not contain any journey")?[0],
        );
        Ok(journey)
    }

    #[allow(non_snake_case)]
    pub async fn stationboard(
        &self,
        stationboard_config: &StationBoardConfig,
    ) -> Result<Vec<StationBoard>, BoxError> {
        let station_id: i64 = stationboard_config
            .station_id
            .lock()
            .unwrap()
            .ok_or("stationboard_config.station_id does not contain a Location id")?;

        let mut bitmask_sum: u64 = 0;
        for product_used in stationboard_config.products_used.iter() {
            if *product_used.used.lock().unwrap() {
                bitmask_sum += product_used.product.bitmasks[0];
            }
        }

        let date = Some(
            stationboard_config
                .datetime
                .lock()
                .unwrap()
                .unwrap_or(Local::now())
                .format("%Y%m%d")
                .to_string(),
        );
        let time = Some(
            stationboard_config
                .datetime
                .lock()
                .unwrap()
                .unwrap_or(Local::now())
                .format("%H%M%S")
                .to_string(),
        );

        let datetime_type = match *stationboard_config.datetime_type.lock().unwrap() {
            TimeType::Departure => "DEP".to_string(),
            TimeType::Arrival => "ARR".to_string(),
        };

        let mut request = Rec::new();
        request.r#type = Some(datetime_type);
        request.stbLoc = Some(StbLoc {
            lid: None,
            extId: Some(station_id.to_string()),
            state: Some("F".to_string()),
            r#type: Some("S".to_string()),
        });
        request.jnyFltrL = Some(vec![JnyFltrL {
            r#type: "PROD".to_string(),
            mode: "INC".to_string(),
            value: bitmask_sum.to_string(),
        }]);
        request.date = date;
        request.time = time;
        request.maxJny = *stationboard_config.results.lock().unwrap();

        let mut post_config = PostConfig::new(request, self.clone());
        post_config.svcReqL[0].meth = Some("StationBoard".to_string());

        let server_reply = self.server_request(post_config).await?;

        let stationboard = StationBoard::new_stationboard(
            &server_reply
                .svcResL
                .as_ref()
                .ok_or("No stationboard data found in server response")?[0],
        )?;

        Ok(stationboard)
    }

    // TODO: split in journey_request, get_earlier, get_later and get_journeys?
    pub async fn journeys(
        &self,
        journeys_config: &JourneysConfig,
        journeys_ref: Option<String>,
    ) -> Result<Journeys, BoxError> {
        let origin = {
            let origin_option_ref = &*journeys_config.departure.lock().unwrap();
            let origin_option = origin_option_ref.clone();
            origin_option
                .ok_or("endpoint.journeys_config.departure does not contain a Location id")?
        };

        let destination = {
            let destination_option_ref = &*journeys_config.arrival.lock().unwrap();
            let destination_option = destination_option_ref.clone();
            destination_option
                .ok_or("endpoint.journeys_config.arrival does not contain a Location id")?
        };

        let mut bitmask_sum: u64 = 0;
        for product_used in journeys_config.products_used.iter() {
            if *product_used.used.lock().unwrap() {
                bitmask_sum += product_used.product.bitmasks[0];
            }
        }

        let out_date: Option<String>;
        let out_time: Option<String>;
        let journeys_ref2: Option<String>;

        match &journeys_ref {
            Some(r) => {
                out_date = None;
                out_time = None;
                if r == &"earlierRef".to_string() {
                    journeys_ref2 = match journeys_config.earlier_ref.lock().unwrap().clone() {
                        Some(jr) => Some(jr),
                        None => {
                            let error_msg: BoxError =
                                "no  let err earlierRef in journeys_config found".into();
                            return Err(error_msg);
                        }
                    }
                } else if r == &"laterRef".to_string() {
                    journeys_ref2 = match journeys_config.later_ref.lock().unwrap().clone() {
                        Some(jr) => Some(jr),
                        None => {
                            let error_msg: BoxError = "no laterRef in journeys_config found".into();
                            return Err(error_msg);
                        }
                    }
                } else {
                    println!("unkown value for journeys_ref: {}", r);
                    let error_msg: BoxError = "unkown value for journeys_ref".into();
                    return Err(error_msg);
                }
            }
            None => {
                out_date = Some(
                    journeys_config
                        .datetime
                        .lock()
                        .unwrap()
                        .unwrap_or(Local::now())
                        .format("%Y%m%d")
                        .to_string(),
                );
                out_time = Some(
                    journeys_config
                        .datetime
                        .lock()
                        .unwrap()
                        .unwrap_or(Local::now())
                        .format("%H%M%S")
                        .to_string(),
                );
                journeys_ref2 = None;
            }
        }

        let datetime_type = match *journeys_config.datetime_type.lock().unwrap() {
            TimeType::Departure => true,
            TimeType::Arrival => false,
        };

        let mut request = Rec::new();

        let mut via_loc_l = Vec::new();
        let via = journeys_config.via.lock().unwrap().clone();
        for s in via.clone().iter() {
            let via_loc_l_row = match s.clone().location_type {
                Some(lt) => match lt.as_str() {
                    "S" => {
                        ViaLocL {
                            loc: Loc {
                                r#type: s.clone().location_type.unwrap(),
                                // lid: Some("A=1@L=".to_string() + &s.clone().id.ok_or("no location id for via location")? + &"@".to_string()), // TODO
                                lid: None,
                                name: None,
                                extId: Some(s.clone().id.ok_or("no location id for via location")?),
                            },
                        }
                    }
                    _ => ViaLocL {
                        loc: Loc {
                            r#type: s.clone().location_type.unwrap_or("A".to_string()),
                            lid: Some(s.clone().id.ok_or("no location id for via location")?),
                            name: None,
                            extId: None,
                        },
                    },
                },
                _ => ViaLocL {
                    loc: Loc {
                        r#type: s.clone().location_type.unwrap_or("A".to_string()),
                        lid: Some(s.clone().id.ok_or("no location id for via location")?),
                        name: None,
                        extId: None,
                    },
                },
            };
            via_loc_l.push(via_loc_l_row);
        }

        match &origin.clone().location_type {
            Some(lt) => match lt.as_str() {
                "S" => {
                    request.depLocL = Some(vec![DepLocL {
                        r#type: "S".to_string(), // type: station
                        lid: None,
                        extId: Some(
                            origin
                                .id
                                .as_ref()
                                .ok_or("no location id for origin location")?
                                .to_string(),
                        ),
                    }]);
                }
                _ => {
                    request.depLocL = Some(vec![DepLocL {
                        r#type: origin.clone().location_type.unwrap_or("A".to_string()),
                        lid: Some(
                            origin
                                .id
                                .as_ref()
                                .ok_or("no location id for origin location")?
                                .to_string(),
                        ),
                        extId: None,
                    }]);
                }
            },
            _ => {
                request.depLocL = Some(vec![DepLocL {
                    r#type: origin.clone().location_type.unwrap_or("A".to_string()),
                    lid: Some(
                        origin
                            .id
                            .as_ref()
                            .ok_or("no location id for origin location")?
                            .to_string(),
                    ),
                    extId: None,
                }]);
            }
        }

        match destination.clone().location_type {
            Some(lt) => match lt.as_str() {
                "S" => {
                    request.arrLocL = Some(vec![ArrLocL {
                        r#type: "S".to_string(), // type: station
                        lid: None,
                        extId: Some(
                            destination
                                .id
                                .as_ref()
                                .ok_or("no location id for origin location")?
                                .to_string(),
                        ),
                    }]);
                }
                _ => {
                    request.arrLocL = Some(vec![ArrLocL {
                        r#type: destination.clone().location_type.unwrap_or("A".to_string()),
                        lid: Some(
                            destination
                                .id
                                .as_ref()
                                .ok_or("no location id for destination location")?
                                .to_string(),
                        ),
                        extId: None,
                    }]);
                }
            },
            _ => {
                request.arrLocL = Some(vec![ArrLocL {
                    r#type: destination.clone().location_type.unwrap_or("A".to_string()),
                    lid: Some(
                        destination
                            .id
                            .as_ref()
                            .ok_or("no location id for destination location")?
                            .to_string(),
                    ),
                    extId: None,
                }]);
            }
        }

        request.viaLocL = Some(via_loc_l);
        request.jnyFltrL = Some(vec![JnyFltrL {
            r#type: "PROD".to_string(),
            mode: "INC".to_string(),
            value: bitmask_sum.to_string(),
        }]);
        request.outDate = out_date;
        request.outTime = out_time;
        request.outFrwd = Some(datetime_type);
        request.ctxScr = journeys_ref2;
        request.minChgTime = *journeys_config.transfer_time.lock().unwrap();
        request.maxChg = Some(journeys_config.max_transfers.lock().unwrap().unwrap_or(-1));
        request.numF = Some(journeys_config.results.lock().unwrap().unwrap_or(-1));
        request.getPasslist = Some(true);
        request.getPolyline = Some(true);

        let mut post_config = PostConfig::new(request, self.clone());
        post_config.svcReqL[0].meth = Some("TripSearch".to_string());

        let server_reply = self.server_request(post_config).await?;

        let journeys = Journeys::new(
            &server_reply
                .svcResL
                .as_ref()
                .ok_or("No Journey data found in server response")?[0]
                .res,
        )?;

        match &journeys_ref {
            Some(r) => {
                if r == &"earlierRef".to_string() {
                    *journeys_config.earlier_ref.lock().unwrap() = journeys.clone().earlierRef;
                } else if r == &"laterRef".to_string() {
                    *journeys_config.later_ref.lock().unwrap() = journeys.clone().laterRef;
                }
            }
            None => {
                *journeys_config.earlier_ref.lock().unwrap() = journeys.clone().earlierRef;
                *journeys_config.later_ref.lock().unwrap() = journeys.clone().laterRef;
            }
        }

        Ok(journeys)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;
    use std::sync::{Arc, Mutex};

    pub static RUNTIME: Lazy<tokio::runtime::Runtime> =
        Lazy::new(|| tokio::runtime::Runtime::new().unwrap());

    #[test]
    fn test_send_trait() {
        let endpoint = Endpoint::infos().unwrap()[0].clone();
        let endpoint2 = endpoint.clone();

        let mut get_journey_config = JourneysConfig::default(&endpoint);
        get_journey_config.results = Arc::new(Mutex::new(Some(5)));

        let mut departure = Location::default();
        departure.id = Some(8000105.to_string());
        get_journey_config.departure = Arc::new(Mutex::new(Some(departure)));
        let mut arrival = Location::default();
        arrival.id = Some(8000001.to_string());
        get_journey_config.arrival = Arc::new(Mutex::new(Some(arrival)));

        let _journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> =
            RUNTIME.spawn(async move {
                let _journeys = endpoint.journeys(&get_journey_config, None).await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                _journeys
            });

        let locations_config = LocationsConfig::default();

        let _locations_handle: tokio::task::JoinHandle<Result<Vec<Location>, BoxError>> = RUNTIME
            .spawn(async move {
                let _locations = endpoint2
                    .locations(&"Frankfurt".to_string(), &locations_config)
                    .await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                _locations
            });
        println!("Locationshandle: {:#?}", _locations_handle);
    }

    #[test]
    fn get_journeys_test() {
        get_journeys_async();
    }

    #[tokio::main]
    async fn get_journeys_async() {
        let endpoint = &Endpoint::infos().unwrap()[0];

        let mut get_journey_config = JourneysConfig::default(&endpoint);
        // get_journey_config.from = 8000105;    // Frankfurt Hbf
        // get_journey_config.from = "8070704".parse::<i64>().unwrap_or(0);    // Aachen Schanz
        // get_journey_config.via = Some(vec!(8000134));    // Trier Hbf
        // get_journey_config.to = 8000001;  // Aachen Hbf
        // get_journey_config.datetime = Some(Local.ymd(2021, 5, 25).and_hms(20, 0, 0));
        // get_journey_config.products_used.get_mut("express-train").unwrap().used = false;
        get_journey_config.results = Arc::new(Mutex::new(Some(5)));
        let mut departure = Location::default();
        departure.id = Some(8000105.to_string());
        departure.location_type = Some("S".to_string());
        get_journey_config.departure = Arc::new(Mutex::new(Some(departure)));
        let mut arrival = Location::default();
        arrival.id = Some(8000001.to_string());
        arrival.location_type = Some("S".to_string());
        get_journey_config.arrival = Arc::new(Mutex::new(Some(arrival)));

        let journeys = endpoint.journeys(&get_journey_config, None).await;
        match journeys {
            Ok(j) => println!("result: {:#?}", j),
            Err(e) => panic!("Error: {}", e),
        }
        //  assert_eq!(result, Err);
    }

    #[test]
    fn get_stationboard_test() {
        env_logger::init();
        get_stationboard_async();
    }

    #[tokio::main]
    async fn get_stationboard_async() {
        let endpoint = &Endpoint::infos().unwrap()[0];

        let mut stationboard_config = StationBoardConfig::default(&endpoint);
        // get_journey_config.datetime = Some(Local.ymd(2021, 5, 25).and_hms(20, 0, 0));
        // get_journey_config.products_used.get_mut("express-train").unwrap().used = false;
        // get_journey_config.results = Arc::new(Mutex::new(Some(5)));
        stationboard_config.station_id = Arc::new(Mutex::new(Some(8000001)));
        stationboard_config.datetime_type = Arc::new(Mutex::new(TimeType::Arrival));

        let stationboard = endpoint.stationboard(&stationboard_config).await;
        match stationboard {
            Ok(j) => println!("result: {:#?}", j),
            Err(e) => panic!("Error: {}", e),
        }
        //  assert_eq!(result, Err);
    }

    #[test]
    fn include_endpoint_from_file_test() {
        let endpoints = Endpoint::infos().unwrap(); //fails if file is not readable
        println!("{:#?}", endpoints);
    }
}
