use serde::{Deserialize, Serialize};

// Structs necessary to deserialize server response

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ServerReply {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cInfo: Option<CInfo>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ext: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    pub lang: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub svcResL: Option<Vec<SvcResL>>,
    pub ver: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CInfo {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub code: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub msg: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SvcResL {
    pub err: String,
    pub meth: String,
    pub res: Res,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Res {
    pub common: Common,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fpB: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fpE: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub jnyL: Option<Vec<JnyL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outConL: Option<Vec<OutConL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub planrtTS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sD: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sT: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#match: Option<Match>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outCtxScrB: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outCtxScrF: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Match {
    pub locL: Vec<LocL>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Arr {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aOutR: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPltfR: Option<PltfR>, // TODO
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPltfS: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTZOffset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub idx: Option<i32>,
    pub locX: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum PltfR {
    PlatformString(String),
    PlatformStruct(APltfRStruct),
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct APltfRStruct {
    pub txt: String,
    pub r#type: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Dep {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dInR: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPltfR: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPltfS: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dProdX: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dProgType: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTZOffset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub idx: Option<i32>,
    pub locX: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SDays {
    pub sDaysB: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sDaysR: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SecL {
    pub arr: Arr,
    pub dep: Dep,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icoX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub jny: Option<Jny>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parJnyL: Option<Vec<Jny>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resRecommendation: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resState: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Jny {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ctxRecon: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dirTxt: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub isRchbl: Option<bool>,
    pub jid: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub msgL: Option<Vec<MsgL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub polyG: Option<PolyG>,
    pub prodX: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stopL: Option<Vec<StopL>>,
    pub subscr: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PolyG {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crdSysX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub layerX: Option<i32>,
    pub polyXL: Vec<i32>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MsgL {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fIdx: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fLocX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub remX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tIdx: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tLocX: Option<i32>,
    pub tagL: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SotCtxt {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub calcDate: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub calcTime: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cnLocX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub jid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locMode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pLocX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reqMode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sectX: Option<i32>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct OutConL {
    pub arr: Arr,
    pub chg: i32,
    pub cid: String,
    pub cksum: String,
    pub conSubscr: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ctxRecon: Option<String>,
    pub date: String,
    pub dep: Dep,
    pub dur: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub isSotCon: Option<bool>,
    pub recState: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resRecommendation: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resState: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sDays: Option<SDays>,
    pub secL: Vec<SecL>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub showARSLink: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sotCtxt: Option<SotCtxt>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sotRating: Option<i32>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Common {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crdSysL: Option<Vec<CrdSysL>>,
    pub icoL: Vec<IcoL>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub layerL: Option<Vec<LayerL>>, // is it a Vec?
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locL: Option<Vec<LocL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub opL: Option<Vec<OpL>>,
    pub polyL: Option<Vec<PolyL>>, // only for db endpoint
    pub prodL: Option<Vec<ProdL>>,
    pub remL: Option<Vec<RemL>>, // only for db endpoint
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RemL {
    pub code: String,
    pub icoX: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prio: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub txtN: Option<String>, // i. e. "Baumaßnahmen"
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PolyL {
    pub crdEncF: String,
    pub crdEncYX: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crdEncZ: Option<String>,
    pub delta: bool,
    pub dim: i32,
    pub ppLocRefL: Vec<PpLocRefL>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PpLocRefL {
    pub locX: i32,
    pub ppIdx: i32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CrdSysL {
    pub dim: i32,
    pub id: String,
    pub index: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct IcoL {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub res: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub txt: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LayerL {
    pub annoCnt: i32,
    pub id: String,
    pub index: i32,
    pub name: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LocL {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crd: Option<Crd>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extId: Option<String>,
    pub icoX: i32,
    pub lid: String,
    pub name: String, // Göttigen
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pCls: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pRefL: Option<Vec<i32>>,
    pub state: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Crd {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub crdSysX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub layerX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    pub x: f64,
    pub y: f64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub z: Option<u64>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct OpL {
    pub icoX: i32,
    pub name: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ProdL {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cls: Option<i32>,
    pub icoX: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub number: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub oprX: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prodCtx: Option<ProdCtx>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ProdCtx {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub admin: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub catCode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub catIn: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub catOut: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub catOutL: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub catOutS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub matchId: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub num: Option<String>,
}

// Journey_leg
#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct JnyL {
    pub date: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dirFlg: Option<String>,
    pub dirTxt: String,
    pub isRchbl: bool,
    pub jid: String,
    pub prodX: usize,
    pub status: String,
    pub stbStop: StbStop,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stopL: Option<Vec<StopL>>,
    pub subscr: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StbStop {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dInR: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dProdX: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTZOffset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub idx: Option<i32>,
    pub locX: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StopL {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dInR: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPltfS: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dPltfR: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTZOffset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aOutR: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPlatfR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPltfS: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aPltfR: Option<PltfR>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTZOffset: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeS: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aTimeR: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aCncl: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dDirTxt: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub idx: Option<i32>,
    pub locX: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}
