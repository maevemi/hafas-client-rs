use crate::Endpoint;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

// Structs to create the body to call the server

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Auth {
    #[serde(skip_serializing_if = "Option::is_none")]
    r#type: Option<String>,
    aid: String, // endpoint-specific authentication token, e.g. `1Rxs112shyHLatUX4fofnmdxK`
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ClientR {
    r#type: String, // might also be `IPH` for "iPhone" or `WEB` for "web client"
    id: String,     // endpoint-specific string, e.g. `BVG`
    #[serde(skip_serializing_if = "Option::is_none")]
    name: Option<String>, // endpoint-specific string, e.g. `FahrInfo`
    #[serde(skip_serializing_if = "Option::is_none")]
    v: Option<u64>, // endpoint-specific string, e.g. `4070700`
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Cfg {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cfgGrpL: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cfgHash: Option<String>, // endpoint-specific string
    #[serde(skip_serializing_if = "Option::is_none")]
    pub polyEnc: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SvcReql {
    pub meth: Option<String>, // name of the API call, supported values depend on the endpoint
    req: Rec,                 // actual request parameters…
    // some endpoints also require this:
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cfg: Option<Cfg>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Response {
    data: String,
    method: String,
    headers: HashMap<String, String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StbLoc {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extId: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct JnyFltrL {
    pub r#type: String,
    pub mode: String,
    pub value: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct Ctx {
    pub ctx: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct Input {
    pub(crate) field: String,
    pub(crate) loc: Loc,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct ViaLocL {
    pub(crate) loc: Loc,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct Loc {
    pub(crate) r#type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) lid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extId: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct DepLocL {
    pub r#type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extId: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct ArrLocL {
    pub r#type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extId: Option<String>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct PostConfig {
    pub auth: Auth,
    pub ver: Option<String>, // endpoint-specific string, e.g. `1.15`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ext: Option<String>, // endpoint-specific string, e.g. `BVG.1`
    pub client: ClientR,
    pub lang: String, // language, sometimes 2-digit (e.g. `de`), sometimes 3-digit (e.g. `deu`)
    pub svcReqL: Vec<SvcReql>,
}

impl PostConfig {
    pub fn new(rec: Rec, endpoint_info: Endpoint) -> Self {
        PostConfig {
            auth: Auth {
                r#type: endpoint_info.options.auth.r#type,
                aid: endpoint_info.options.auth.aid,
            },
            ver: endpoint_info.options.version,
            ext: endpoint_info.options.ext,
            client: endpoint_info.options.client,
            lang: endpoint_info.supportedLanguages[0].clone(),
            svcReqL: vec![SvcReql {
                meth: None,
                req: rec, // actual request parameters…
                // some endpoints also require this:
                cfg: None,
            }],
        }
    }
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub(crate) struct Rec {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stbLoc: Option<StbLoc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dirLoc: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maxJny: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dur: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub jnyFltrL: Option<Vec<JnyFltrL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub arrLocL: Option<Vec<ArrLocL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub viaLocL: Option<Vec<ViaLocL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub depLocL: Option<Vec<DepLocL>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outDate: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outTime: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outFrwd: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ctxScr: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub minChgTime: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maxChg: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub numF: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub input: Option<Input>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ctxRecon: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub getPasslist: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub getPolyline: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outReconL: Option<Vec<Ctx>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stbFltrEquiv: Option<bool>,
}

impl Rec {
    pub fn new() -> Self {
        Rec {
            r#type: None,
            stbLoc: None,
            dirLoc: None,
            maxJny: None,
            date: None,
            time: None,
            dur: None,
            jnyFltrL: None,
            arrLocL: None,
            viaLocL: None,
            depLocL: None,
            outDate: None,
            outTime: None,
            outFrwd: None,
            ctxScr: None,
            minChgTime: None,
            maxChg: None,
            numF: None,
            input: None,
            ctxRecon: None,
            getPasslist: None,
            getPolyline: None,
            outReconL: None,
            stbFltrEquiv: None,
        }
    }
}
