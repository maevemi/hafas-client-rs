use crate::response::{LocL, OutConL, PltfR, Res, SecL, StopL, SvcResL};
use crate::BoxError;
use chrono::*;
use std::collections::HashMap;

// struct (close to) the Friendly Public Transport Format

#[derive(Debug, Clone, Default)]
pub struct Location {
    pub id: Option<String>,
    pub location_type: Option<String>,
    pub lid: String,
    pub name: String,
    pub coordinates: Coordinates,
}

#[derive(Debug, Clone)]
#[allow(non_snake_case)]
pub struct Leg {
    pub id: Option<String>,
    pub line: Option<String>,
    pub origin: Location,
    pub destination: Location,
    pub cancelled: bool,
    pub departure: Option<NaiveDateTime>,
    pub plannedDeparture: Option<NaiveDateTime>,
    pub arrival: Option<NaiveDateTime>,
    pub plannedArrival: Option<NaiveDateTime>,
    pub departurePlatform: Option<String>,
    pub arrivalPlatform: Option<String>,
    pub direction: Option<String>,
    pub stopovers: Option<Vec<StopOver>>,
}

#[derive(Debug, Clone, Default)]
pub struct Coordinates {
    pub latitude: f64,
    pub longitude: f64,
}

#[derive(Clone, Debug, Default)]
#[allow(non_snake_case)]
pub struct Journeys {
    pub earlierRef: Option<String>,
    pub laterRef: Option<String>,
    pub journeys: Vec<Journey>,
    pub realtimeDataFrom: i64,
}

#[derive(Debug, Clone)]
#[allow(non_snake_case)]
pub struct Journey {
    pub legs: Vec<Leg>,
    pub refreshToken: String,
}

#[derive(Debug, Clone)]
#[allow(non_snake_case)]
pub struct StopOver {
    pub stop: Location,
    pub cancelled: bool,
    pub departure: Option<NaiveDateTime>,
    pub plannedDeparture: Option<NaiveDateTime>,
    pub arrival: Option<NaiveDateTime>,
    pub plannedArrival: Option<NaiveDateTime>,
    pub departurePlatform: Option<String>,
    pub arrivalPlatform: Option<String>,
}

#[derive(Debug, Clone)]
#[allow(non_snake_case)]
pub struct StationBoard {
    pub stop: Option<Location>,
    pub id: String,
    pub name: Option<String>,
    pub direction: Option<String>,
    pub cancelled: Option<bool>,
    pub time: Option<NaiveDateTime>,
    pub plannedTime: Option<NaiveDateTime>,
    pub platform: Option<String>,
    pub stopovers: Option<Vec<StopOver>>,
}

impl StationBoard {
    pub fn new_stationboard(raw_stationboard: &SvcResL) -> Result<Vec<Self>, BoxError> {
        let mut stationboard = Vec::new();
        for raw_entry in raw_stationboard
            .res
            .jnyL
            .clone()
            .ok_or("no stationboard found")?
            .iter()
        {
            if raw_stationboard.res.r#type == Some("DEP".to_string()) {
                let platform = match &raw_entry.stbStop.dPlatfR {
                    Some(p) => Some(p.clone()),
                    None => raw_entry.stbStop.dPlatfS.as_ref().cloned(),
                };

                // TODO move for loop to Stopover function?
                let stopovers = match &raw_entry.stopL {
                    Some(stopovers) => {
                        let mut stopovers_out = Vec::new();
                        for stopover in stopovers.iter() {
                            stopovers_out.push(StopOver::new(&raw_stationboard.res, stopover));
                        }
                        Some(stopovers_out)
                    }
                    None => None,
                };

                let stop = match &raw_stationboard.res.common.locL {
                    Some(locl) => match raw_entry.clone().stopL {
                        Some(stopl) => Some(Location::new(&locl[stopl[0].locX])),
                        None => None,
                    },
                    None => None,
                };

                let name = match raw_stationboard.res.common.prodL.clone() {
                    Some(prodl) => prodl[raw_entry.prodX].name.clone(),
                    None => None,
                };

                let entry = StationBoard {
                    stop,
                    id: raw_entry.jid.clone(),
                    name,
                    direction: Some(raw_entry.dirTxt.clone()),
                    cancelled: raw_entry.stbStop.dCncl,
                    time: parse_datetime(&raw_entry.stbStop.dTimeR, &raw_entry.date),
                    plannedTime: parse_datetime(&raw_entry.stbStop.dTimeS.clone(), &raw_entry.date),
                    platform,
                    stopovers,
                };
                stationboard.push(entry);
            } else {
                let platform = match &raw_entry.stbStop.aPlatfR {
                    Some(p) => Some(p.clone()),
                    None => raw_entry.stbStop.aPlatfS.as_ref().cloned(),
                };

                let stopovers = match &raw_entry.stopL {
                    Some(stopovers) => {
                        let mut stopovers_out = Vec::new();
                        for stopover in stopovers.iter() {
                            stopovers_out.push(StopOver::new(&raw_stationboard.res, stopover));
                        }
                        Some(stopovers_out)
                    }
                    None => None,
                };

                let stop = match &raw_stationboard.res.common.locL {
                    Some(locl) => match raw_entry.clone().stopL {
                        Some(stopl) => Some(Location::new(&locl[stopl[stopl.len() - 1].locX])),
                        None => None,
                    },
                    None => None,
                };

                let name = match raw_stationboard.res.common.prodL.clone() {
                    Some(prodl) => prodl[raw_entry.prodX].name.clone(),
                    None => None,
                };

                let entry = StationBoard {
                    stop,
                    id: raw_entry.jid.clone(),
                    name,
                    direction: Some(raw_entry.dirTxt.clone()),
                    cancelled: raw_entry.stbStop.aCncl,
                    time: parse_datetime(&raw_entry.stbStop.aTimeR, &raw_entry.date),
                    plannedTime: parse_datetime(&raw_entry.stbStop.aTimeS.clone(), &raw_entry.date),
                    platform,
                    stopovers,
                };
                stationboard.push(entry);
            }
        }
        Ok(stationboard)
    }
}

// TODO: have only one impl for Location
impl Location {
    pub fn new_locations(raw_locations: &[LocL]) -> Vec<Self> {
        let mut locations = Vec::new();
        for location in raw_locations.iter() {
            locations.push(Location::new(&location.clone()));
        }
        locations
    }
}

impl Journeys {
    pub fn new(journeys_raw: &Res) -> Result<Self, BoxError> {
        let mut journeys = Vec::new();
        for journey in journeys_raw
            .outConL
            .as_ref()
            .ok_or("journeys_raw.outConL does not contain any journeys")?
            .iter()
        {
            journeys.push(Journey::new(journeys_raw, journey));
        }

        Ok(Journeys {
            earlierRef: journeys_raw.outCtxScrB.clone(),
            laterRef: journeys_raw.outCtxScrF.clone(),
            journeys,
            realtimeDataFrom: 0, //TODO
        })
    }
}

impl Journey {
    pub fn new(journeys: &Res, journey: &OutConL) -> Self {
        let mut legs = Vec::new();
        for leg in journey.clone().secL.iter() {
            legs.push(Leg::new(journeys, leg));
        }

        Journey {
            legs,
            refreshToken: journey
                .ctxRecon
                .as_ref()
                .unwrap_or(&"no refresh token found".to_string())
                .clone(), //TODO implement proper error
        }
    }
}

impl Leg {
    fn new(journeys: &Res, leg: &SecL) -> Self {
        let departure_pl = match &leg.dep.dPlatfR {
            Some(p) => Some(p.clone()),
            None => match &leg.dep.dPlatfS {
                Some(p) => Some(p.clone()),
                None => match &leg.dep.dPltfR {
                    Some(p) => match p {
                        PltfR::PlatformString(p) => Some(p.clone()),
                        PltfR::PlatformStruct(p) => Some(p.clone().txt),
                    },
                    None => match &leg.dep.dPltfS {
                        Some(p) => match p {
                            PltfR::PlatformString(p) => Some(p.clone()),
                            PltfR::PlatformStruct(p) => Some(p.clone().txt),
                        },
                        None => None,
                    },
                },
            },
        };
        let arrival_pl = match &leg.arr.aPlatfR {
            Some(p) => Some(p.clone()),
            None => match &leg.arr.aPlatfS {
                Some(p) => Some(p.clone()),
                None => match &leg.arr.aPltfR {
                    Some(p) => match p {
                        PltfR::PlatformString(p) => Some(p.clone()),
                        PltfR::PlatformStruct(p) => Some(p.clone().txt),
                    },
                    None => match &leg.arr.aPltfS {
                        Some(p) => match p {
                            PltfR::PlatformString(p) => Some(p.clone()),
                            PltfR::PlatformStruct(p) => Some(p.clone().txt),
                        },
                        None => None,
                    },
                },
            },
        };
        let line = match &leg.jny {
            Some(jny) => match journeys.common.prodL.clone() {
                Some(prodl) => prodl[jny.prodX].name.as_ref().cloned(),
                None => None,
            },
            None => None,
        };

        let direction = match &leg.jny {
            Some(jny) => jny.dirTxt.as_ref().cloned(),
            None => None,
        };
        let id = leg.jny.as_ref().map(|jny| jny.jid.clone());
        let stopovers = match &leg.jny {
            // TODO: also check for stopovers in parJnyL?
            Some(jny) => match &jny.stopL {
                Some(stopovers) => {
                    let mut stopovers_out = Vec::new();
                    for stopover in stopovers.iter() {
                        stopovers_out.push(StopOver::new(journeys, stopover));
                    }
                    stopovers_out.remove(0);
                    stopovers_out.remove(stopovers_out.len() - 1);
                    Some(stopovers_out)
                }
                None => None,
            },
            None => None,
        };

        let date = &journeys.outConL.as_ref().unwrap()[0].date; // TODO handle unwrap failure?

        Leg {
            id,
            line,
            origin: Location::new(&journeys.common.locL.as_ref().unwrap()[leg.dep.locX]),
            destination: Location::new(&journeys.common.locL.as_ref().unwrap()[leg.arr.locX]),
            cancelled: leg.arr.aCncl.unwrap_or(false),
            departure: parse_datetime(&leg.dep.dTimeR, date),
            plannedDeparture: parse_datetime(&leg.dep.dTimeS, date),
            arrival: parse_datetime(&leg.arr.aTimeR, date),
            plannedArrival: parse_datetime(&leg.arr.aTimeS, date),
            departurePlatform: departure_pl,
            arrivalPlatform: arrival_pl,
            direction,
            stopovers,
        }
    }
}

impl StopOver {
    #[allow(non_snake_case)]
    fn new(journeys: &Res, stopover: &StopL) -> Self {
        let date = match &journeys.outConL {
            Some(o) => o[0].date.clone(),
            None => journeys.jnyL.clone().unwrap()[0].date.clone(), // TODO handle unwrap failure?
        };

        let stop = Location::new(&journeys.common.locL.as_ref().unwrap()[stopover.locX]);

        let cancelled = match stopover.dCncl {
            Some(bool) => bool,
            None => stopover.aCncl.unwrap_or(false),
        };

        // TODO create own function and use for leg parsing too
        let departurePlatform = match &stopover.dPlatfR {
            Some(p) => Some(p.clone()),
            None => match &stopover.dPlatfS {
                Some(p) => Some(p.clone()),
                None => match &stopover.dPltfR {
                    Some(p) => match p {
                        PltfR::PlatformString(p) => Some(p.clone()),
                        PltfR::PlatformStruct(p) => Some(p.clone().txt),
                    },
                    None => match &stopover.dPltfS {
                        Some(p) => match p {
                            PltfR::PlatformString(p) => Some(p.clone()),
                            PltfR::PlatformStruct(p) => Some(p.clone().txt),
                        },
                        None => None,
                    },
                },
            },
        };
        let arrivalPlatform = match &stopover.aPlatfR {
            Some(p) => Some(p.clone()),
            None => match &stopover.aPlatfS {
                Some(p) => Some(p.clone()),
                None => match &stopover.aPltfR {
                    Some(p) => match p {
                        PltfR::PlatformString(p) => Some(p.clone()),
                        PltfR::PlatformStruct(p) => Some(p.clone().txt),
                    },
                    None => match &stopover.aPltfS {
                        Some(p) => match p {
                            PltfR::PlatformString(p) => Some(p.clone()),
                            PltfR::PlatformStruct(p) => Some(p.clone().txt),
                        },
                        None => None,
                    },
                },
            },
        };

        StopOver {
            stop,
            cancelled,
            departure: parse_datetime(&stopover.dTimeR, &date),
            plannedDeparture: parse_datetime(&stopover.dTimeS, &date),
            arrival: parse_datetime(&stopover.aTimeR, &date),
            plannedArrival: parse_datetime(&stopover.aTimeS, &date),
            departurePlatform,
            arrivalPlatform,
        }
    }
}

impl Location {
    fn new(raw_location: &LocL) -> Self {
        let lid = raw_location.lid.clone();

        let mut lid_hashmap = HashMap::new();
        let value_pairs = lid.split('@');
        for value_pair in value_pairs {
            let value = value_pair.split('=');
            let value_pair_vec: Vec<&str> = value.collect();
            if value_pair_vec.len() == 2 {
                lid_hashmap.insert(value_pair_vec[0], value_pair_vec[1]);
            }
        }

        let id = match raw_location.r#type.clone() {
            Some(t) => {
                let location_type = match t.as_str() {
                    "S" => Some(lid_hashmap["L"].to_string()),
                    _ => Some(lid.clone()),
                };
                location_type
            }
            None => None,
        };

        Location {
            id,
            location_type: raw_location.r#type.clone(),
            lid: lid.clone(),
            name: lid_hashmap["O"].to_string(),
            coordinates: Coordinates {
                latitude: lid_hashmap["Y"].parse::<f64>().unwrap_or(0.0) / 1000000.0,
                longitude: lid_hashmap["X"].parse::<f64>().unwrap_or(0.0) / 1000000.0,
            },
        }
    }
}

fn parse_datetime(time: &Option<String>, date: &str) -> Option<NaiveDateTime> {
    match time {
        Some(t) => {
            // if a departure is the next day, hafas adds a 01 in front of the time string
            if t.len() == 8 {
                let date_time_str = String::from(date) + &t[2..];
                Some(
                    NaiveDateTime::parse_from_str(&date_time_str, "%Y%m%d%H%M%S").unwrap()
                        + chrono::Duration::days(1),
                )
            } else {
                let date_time_str = String::from(date) + t;
                Some(NaiveDateTime::parse_from_str(&date_time_str, "%Y%m%d%H%M%S").unwrap())
            }
        }
        None => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_datetime_test() {
        let date1 = "20210519".to_string();
        let time1 = Some("134455".to_string());
        let dt1 = parse_datetime(&time1, &date1);
        assert_eq!(
            dt1,
            Some(NaiveDate::from_ymd(2021, 05, 19).and_hms(13, 44, 55))
        );
        println!("Datetime 1: {:?}", dt1);

        let date2 = "20210519".to_string();
        let time2 = Some("01134455".to_string());
        let dt2 = parse_datetime(&time2, &date2);
        assert_eq!(
            dt2,
            Some(NaiveDate::from_ymd(2021, 05, 20).and_hms(13, 44, 55))
        );
        println!("Datetime2: {:?}", dt2);

        let date3 = "20210519".to_string();
        let time3 = None;
        let dt3 = parse_datetime(&time3, &date3);
        assert_eq!(dt3, None);
        println!("Datetime3: {:?}", dt3);
    }

    #[test]
    fn parse_location_test() {
        let locl = LocL {
            crd: None,
            extId: None,
            icoX: 0,
            lid: r#"A=1@O=Begau Siedlung, Alsdorf (Rheinland)@X=6201262@Y=50855789@U=80@L=585456@"#
                .to_string(),
            name: "Testname".to_string(),
            pCls: None,
            pRefL: None,
            state: "Teststate".to_string(),
            r#type: Some("S".to_string()),
        };
        let parsed_location = Location::new(&locl);

        assert_eq!(parsed_location.id, Some("585456".to_string()));
        assert_eq!(parsed_location.coordinates.latitude, f64::from(50.855789));
        assert_eq!(parsed_location.coordinates.longitude, f64::from(6.201262));
        assert_eq!(
            parsed_location.name,
            "Begau Siedlung, Alsdorf (Rheinland)".to_string()
        );
        println!("Location: {:#?}", &parsed_location);
    }
}
